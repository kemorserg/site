<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Collection;

class PagesController extends Controller
{
    public function home() {
        return view('pages.home');
    }

    public function contact() {
        return view('pages.contact');
    }
}

interface SubjectInterface {
    public function registerObserver(ObserverInterface $observer);
    public function removeObserver(ObserverInterface $observer);
    public function notifyObservers();
}

interface ObserverInterface {
    public function update($length, $height, $width);
}

interface DisplayInterface {
    public function display();
}

class WeatherData implements SubjectInterface {
    private $observers;
    private $length;
    private $height;
    private $width;

    public function __construct() {
        $this->observers = new Collection();
    }

    public function registerObserver(ObserverInterface $observer) {
        $this->observers->push($observer);
    }

    public function removeObserver(ObserverInterface $observer) {
        $elem = $this->observers->search($observer, true);
        if ($elem) {
            $this->observers->forget($elem);
        }
    }

    public function notifyObservers() {
        /**
         * @var \App\Http\Controllers\ObserverInterface $observer
         */
        foreach ($this->observers->all() as $observer) {
            $observer->update($this->length, $this->height, $this->width);
        }
    }

    public function measurementChanged() {
        $this->notifyObservers();
    }

    public function setMeasurements($length, $height, $width) {
        $this->length = $length;
        $this->height = $height;
        $this->width = $width;
        $this->measurementChanged();
    }

}

class CurrentConditionDisplay implements DisplayInterface {
    public function display() {
        echo '$length = ' . $this->length . ' $this->' . $this->he . '';
    }

}